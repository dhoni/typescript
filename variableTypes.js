"use strict";
exports.__esModule = true;
var isBeginner = true;
var total = 0;
var name = 'Ansher';
// multiline string
var sentence = "this is a " + name + "\nI am a beginner in typescript";
console.log(sentence);
// null & undefined
var n = null;
var u = undefined;
var isNew = null;
var myName = undefined;
// Array
var list1 = [1, 2, 3];
var list2 = [1, 2, 3];
// tupules - mixed array
// Order of values has to match the types
var person1 = ['Sachin', 10];
// Enum types
// set of numeric values
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
;
var c = Color.Green;
console.log(c);
// Enums by default begins from 1
// To set custom values for enum
// Top states in literacy
var State;
(function (State) {
    State[State["TN"] = 10] = "TN";
    State[State["Kerala"] = 1] = "Kerala";
    State[State["Punjab"] = 3] = "Punjab";
})(State || (State = {}));
;
var mystate = State.TN;
console.log(mystate);
// Any Type
var randomValue = 10;
randomValue = true;
randomValue = 'Sachin';
console.log(randomValue);
