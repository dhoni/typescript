export {}
let isBeginner: boolean = true;
let total: number = 0;
let name: string = 'Ansher';

// multiline string
let sentence: string = `this is a ${name}
I am a beginner in typescript`;

console.log(sentence);

// null & undefined
let n: null = null;
let u: undefined = undefined;

let isNew: boolean = null;
let myName: string = undefined;

// Array
let list1: number[] = [1,2,3];
let list2: Array<number> = [1,2,3];

// tupules - mixed array
// Order of values has to match the types
let person1: [string, number] = ['Sachin', 10];

// Enum types
// set of numeric values
enum Color {Red, Green, Blue};

let c: Color = Color.Green;

console.log(c);

// Enums by default begins from 1
// To set custom values for enum
// Top states in literacy
enum State {TN = 10, Kerala = 1, Punjab = 3};
let mystate: State = State.TN;

console.log(mystate);

// Any Type
let randomValue: any = 10;
randomValue = true;
randomValue = 'Sachin';

console.log(randomValue);





